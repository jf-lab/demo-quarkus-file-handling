# Example to understand the local dir

Some code to understand how QuarkusMainTest and QuarkusMainIntegrationTest works. 

It is actually very easy; it is relative from the maven project dir.  

Check where the files "WorldTest" and "WorldIntegrationTest" are stored.

You can run the test and integration test, by running the following command:

```shell
.\mvnw.cmd verify -DskipITs=false
```

Check also the configuration of the `maven-clean-plugin` in the pom file.