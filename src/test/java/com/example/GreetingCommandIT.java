package com.example;

import io.quarkus.test.junit.main.Launch;
import io.quarkus.test.junit.main.LaunchResult;
import io.quarkus.test.junit.main.QuarkusMainIntegrationTest;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusMainIntegrationTest
public class GreetingCommandIT {

    @Test
    @Launch({"WorldIntegrationTest", "target-it"})
    public void test(LaunchResult result) {
        assertThat(result.exitCode()).isEqualTo(0);
        assertThat(result.getOutput()).contains("WorldIntegrationTest");
        assertThat(result.getOutput()).contains("go go");
    }
}
