package com.example;

import io.quarkus.test.junit.main.Launch;
import io.quarkus.test.junit.main.LaunchResult;
import io.quarkus.test.junit.main.QuarkusMainTest;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


@QuarkusMainTest
class GreetingCommandTest {

    @Test
    @Launch({"WorldTest", "target-test"})
    public void test(LaunchResult result) {
        assertThat(result.exitCode()).isEqualTo(0);
        assertThat(result.getOutput()).contains("World");
        assertThat(result.getOutput()).contains("go go");
    }

}