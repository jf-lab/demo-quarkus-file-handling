package com.example;

import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Command(name = "greeting", mixinStandardHelpOptions = true)
public class GreetingCommand implements Runnable {

    @Parameters(paramLabel = "<name>", defaultValue = "picocli",
        description = "Your name.")
    String name;

    @Parameters(paramLabel = "<folder>", defaultValue = ".", description = "folder to write the file to")
    Path folder;

    @Override
    public void run() {
        System.out.printf("Hello %s, go go commando!\n", name);

        try {
            System.out.println("file written: " + folder);
            Files.createDirectories(folder);
            Files.write(folder.resolve(name), ("Hello " + name).getBytes());
        } catch (IOException e) {
            System.out.println("Unable to write");
            throw new RuntimeException(e);
        }
    }

}
